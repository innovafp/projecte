﻿Imports System.Data.SqlClient
Imports System.Configuration

Public Class Conecta
    Public cnx As SqlConnection
    Public cnxString As String

    Public Sub Conecta()
        cnxString = "Server=InnovaFP-PC;Database=InnovaFP;User Id=sa;Password=12345"
        cnx = New SqlConnection(cnxString)

        cnx.Open()
        cnx.InitializeLifetimeService()
    End Sub

    Public Function GetData(ByVal query As String) As DataSet
        Dim dtaDades As SqlDataAdapter
        Dim construct As SqlCommandBuilder
        Dim dtsDades As DataSet

        Conecta()

        dtaDades = New SqlDataAdapter(query, cnx)
        construct = New SqlCommandBuilder(dtaDades)
        dtsDades = New DataSet

        dtaDades.FillSchema(dtsDades, SchemaType.Source)
        dtaDades.Fill(dtsDades, "Taula")

        GetData = dtsDades

        Return GetData
    End Function

    Public Function GetData(ByVal query As String, ByVal nomtaula As String) As DataSet
        Dim dtaDades As SqlDataAdapter
        Dim dtsDades As DataSet

        dtaDades = New SqlDataAdapter(query, cnx)
        dtsDades = New DataSet

        dtaDades.Fill(dtsDades, nomtaula)

        GetData = dtsDades
    End Function

    Public Function Actualitzar(ByVal query As String, ByVal Taula As String, ByVal dtsAct As DataSet) As Boolean
        Dim correcte As Boolean = True

        'Dim dtsDepart As DataSet
        Dim dtaDepart As SqlDataAdapter
        Dim construct As SqlCommandBuilder

        Try
            dtaDepart = New SqlDataAdapter(query, cnx)
            construct = New SqlCommandBuilder(dtaDepart)
            'dtaDepart.Fill(dtsAct, Taula)
            If (dtsAct.HasChanges) Then
                dtaDepart.Update(dtsAct, Taula)
            End If

        Catch ex As Exception
            correcte = False
        Finally
            If cnx IsNot Nothing Then
                cnx.Close()
                cnx.Dispose()
            End If
        End Try

        Return correcte
    End Function
End Class