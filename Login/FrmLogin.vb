﻿Imports System.Security.Cryptography
Imports System.Text
Imports SdSBBDD
Imports Projectes

Public Class FrmLogin
    Shared usrLogged As Boolean

    Function convertToMd5(aPasswd As String)
        Dim md5 As MD5 = MD5.Create()
        Dim inputBytes As Byte() = Encoding.ASCII.GetBytes(aPasswd)
        Dim hash As Byte() = md5.ComputeHash(inputBytes)
        Dim sb As New StringBuilder()

        For i As Integer = 0 To hash.Length - 1
            sb.Append(hash(i).ToString("x2"))
        Next

        Return sb.ToString()
    End Function

    Private Function campsCorrectes(usuari As String, passwd As String) As Boolean
        Dim correcte As Boolean = False

        If (Not usuari.Equals("") And Not passwd.Equals("")) Then
            MessageBox.Show("Usuaribd: " + usuari + ". Passwordbd: " + passwd)
            correcte = True
        End If

        Return correcte
    End Function

    Private Sub btnEntrar_Click(sender As Object, e As EventArgs) Handles btnEntrar.Click
        Dim usuari As String
        Dim passwd As String
        Dim frmProjectes As New FrmProjectes
        Dim conn As New Conecta()

        usuari = txtUsuari.Text.Trim()
        passwd = txtPasswd.Text.Trim()

        passwd = convertToMd5(passwd) ' Hash the password to MD5 agorithm

        ' Connexió a BD
        conn.GetData("SELECT COUNT(*) AS Num " +
                     "FROM Usuaris " +
                     "WHERE Nom = '" + usuari + "' AND Password = '" + passwd + "'")

        ' Si retorna

        If campsCorrectes(usuari, passwd) Then ' i usuari i contrasenya con correctes (connexio amb BD)

            usrLogged = True

            frmProjectes.ShowDialog()
        Else
            MessageBox.Show("Les dades introduides no són correctes", "Camps buits trobats",
                MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
End Class
