﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLogin))
        Me.txtUsuari = New System.Windows.Forms.TextBox()
        Me.txtPasswd = New System.Windows.Forms.TextBox()
        Me.lblUsuari = New System.Windows.Forms.Label()
        Me.lblPasswd = New System.Windows.Forms.Label()
        Me.btnEntrar = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtUsuari
        '
        Me.txtUsuari.Location = New System.Drawing.Point(123, 115)
        Me.txtUsuari.Name = "txtUsuari"
        Me.txtUsuari.Size = New System.Drawing.Size(100, 20)
        Me.txtUsuari.TabIndex = 1
        '
        'txtPasswd
        '
        Me.txtPasswd.Location = New System.Drawing.Point(123, 141)
        Me.txtPasswd.Name = "txtPasswd"
        Me.txtPasswd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPasswd.Size = New System.Drawing.Size(100, 20)
        Me.txtPasswd.TabIndex = 2
        '
        'lblUsuari
        '
        Me.lblUsuari.AutoSize = True
        Me.lblUsuari.Location = New System.Drawing.Point(64, 114)
        Me.lblUsuari.Name = "lblUsuari"
        Me.lblUsuari.Size = New System.Drawing.Size(37, 13)
        Me.lblUsuari.TabIndex = 3
        Me.lblUsuari.Text = "Usuari"
        '
        'lblPasswd
        '
        Me.lblPasswd.AutoSize = True
        Me.lblPasswd.Location = New System.Drawing.Point(35, 141)
        Me.lblPasswd.Name = "lblPasswd"
        Me.lblPasswd.Size = New System.Drawing.Size(66, 13)
        Me.lblPasswd.TabIndex = 4
        Me.lblPasswd.Text = "Contrasenya"
        '
        'btnEntrar
        '
        Me.btnEntrar.Location = New System.Drawing.Point(102, 176)
        Me.btnEntrar.Name = "btnEntrar"
        Me.btnEntrar.Size = New System.Drawing.Size(86, 23)
        Me.btnEntrar.TabIndex = 5
        Me.btnEntrar.Text = "Iniciar sessió"
        Me.btnEntrar.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(72, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(143, 96)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'FrmLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 220)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btnEntrar)
        Me.Controls.Add(Me.lblPasswd)
        Me.Controls.Add(Me.lblUsuari)
        Me.Controls.Add(Me.txtPasswd)
        Me.Controls.Add(Me.txtUsuari)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmLogin"
        Me.Text = "Inici de sessió"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtUsuari As TextBox
    Friend WithEvents txtPasswd As TextBox
    Friend WithEvents lblUsuari As Label
    Friend WithEvents lblPasswd As Label
    Friend WithEvents btnEntrar As Button
    Friend WithEvents PictureBox1 As PictureBox
End Class
