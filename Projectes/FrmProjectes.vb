﻿Imports System.Windows.Forms

Public Class FrmProjectes
    'cada vez que marques un check:
    'gridMed.Rows(indice).Selected = True
    'pero debes tener el SelectionMode = DataGridViewSelectionMode.FullRowSelect

    ' La primera columna ha de tenir checkboxes

    Public Sub dibuixarTaula()
        Dim i As Integer
        Const AMPLADA_COLUMNA As Integer = 90
        Dim NUM_MAX_COLUMNS As Integer

        lstProjectes.Columns.Add("Seleccionar")
        lstProjectes.Items.Add("Element chk", 0)

        lstProjectes.Columns.Add("Projecte")
        lstProjectes.Items.Item(0).SubItems.Add("S")

        lstProjectes.Columns.Add("Professor")
        lstProjectes.Items.Item(0).SubItems.Add("SA")

        lstProjectes.Columns.Add("Entrega")
        lstProjectes.Items.Item(0).SubItems.Add("SAR")

        NUM_MAX_COLUMNS = lstProjectes.Columns.Count()
        Dim chk(NUM_MAX_COLUMNS) As CheckBox

        ' Display checkboxes
        lstProjectes.CheckBoxes = True

        ' Sort the items in the list in ascending order
        lstProjectes.Sorting = SortOrder.Ascending

        ' Auto fit the width of each column and
        ' create a checkbox for each row
        For i = 0 To NUM_MAX_COLUMNS - 1
            lstProjectes.Columns.Item(i).Width = AMPLADA_COLUMNA
        Next
    End Sub

    Private Sub FrmProjectes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dibuixarTaula()
    End Sub
End Class