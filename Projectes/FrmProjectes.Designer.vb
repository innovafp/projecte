﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmProjectes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmProjectes))
        Me.lstProjectes = New System.Windows.Forms.ListView()
        Me.lblDelete = New System.Windows.Forms.Label()
        Me.lblNew = New System.Windows.Forms.Label()
        Me.pbxDownload = New System.Windows.Forms.PictureBox()
        Me.pbxUpload = New System.Windows.Forms.PictureBox()
        Me.lblDownload = New System.Windows.Forms.Label()
        Me.lblUpload = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.lblUserLogIn = New System.Windows.Forms.Label()
        CType(Me.pbxDownload, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxUpload, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lstProjectes
        '
        Me.lstProjectes.CheckBoxes = True
        Me.lstProjectes.Location = New System.Drawing.Point(12, 59)
        Me.lstProjectes.Name = "lstProjectes"
        Me.lstProjectes.Size = New System.Drawing.Size(347, 190)
        Me.lstProjectes.TabIndex = 4
        Me.lstProjectes.UseCompatibleStateImageBehavior = False
        Me.lstProjectes.View = System.Windows.Forms.View.Details
        '
        'lblDelete
        '
        Me.lblDelete.AutoSize = True
        Me.lblDelete.Location = New System.Drawing.Point(250, 325)
        Me.lblDelete.Name = "lblDelete"
        Me.lblDelete.Size = New System.Drawing.Size(43, 13)
        Me.lblDelete.TabIndex = 7
        Me.lblDelete.Text = "Eliminar"
        '
        'lblNew
        '
        Me.lblNew.AutoSize = True
        Me.lblNew.Location = New System.Drawing.Point(323, 325)
        Me.lblNew.Name = "lblNew"
        Me.lblNew.Size = New System.Drawing.Size(27, 13)
        Me.lblNew.TabIndex = 8
        Me.lblNew.Text = "Nou"
        '
        'pbxDownload
        '
        Me.pbxDownload.BackgroundImage = CType(resources.GetObject("pbxDownload.BackgroundImage"), System.Drawing.Image)
        Me.pbxDownload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbxDownload.Location = New System.Drawing.Point(12, 258)
        Me.pbxDownload.Name = "pbxDownload"
        Me.pbxDownload.Size = New System.Drawing.Size(47, 58)
        Me.pbxDownload.TabIndex = 12
        Me.pbxDownload.TabStop = False
        '
        'pbxUpload
        '
        Me.pbxUpload.BackgroundImage = CType(resources.GetObject("pbxUpload.BackgroundImage"), System.Drawing.Image)
        Me.pbxUpload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbxUpload.Location = New System.Drawing.Point(82, 258)
        Me.pbxUpload.Name = "pbxUpload"
        Me.pbxUpload.Size = New System.Drawing.Size(47, 58)
        Me.pbxUpload.TabIndex = 13
        Me.pbxUpload.TabStop = False
        '
        'lblDownload
        '
        Me.lblDownload.AutoSize = True
        Me.lblDownload.Location = New System.Drawing.Point(5, 325)
        Me.lblDownload.Name = "lblDownload"
        Me.lblDownload.Size = New System.Drawing.Size(65, 13)
        Me.lblDownload.TabIndex = 14
        Me.lblDownload.Text = "Descarregar"
        '
        'lblUpload
        '
        Me.lblUpload.AutoSize = True
        Me.lblUpload.Location = New System.Drawing.Point(88, 325)
        Me.lblUpload.Name = "lblUpload"
        Me.lblUpload.Size = New System.Drawing.Size(31, 13)
        Me.lblUpload.TabIndex = 15
        Me.lblUpload.Text = "Pujar"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Location = New System.Drawing.Point(247, 258)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 58)
        Me.PictureBox1.TabIndex = 16
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = CType(resources.GetObject("PictureBox2.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox2.Location = New System.Drawing.Point(314, 258)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(44, 58)
        Me.PictureBox2.TabIndex = 17
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackgroundImage = CType(resources.GetObject("PictureBox3.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox3.Location = New System.Drawing.Point(13, 4)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox3.TabIndex = 18
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackgroundImage = CType(resources.GetObject("PictureBox4.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox4.Location = New System.Drawing.Point(63, 4)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(49, 48)
        Me.PictureBox4.TabIndex = 19
        Me.PictureBox4.TabStop = False
        Me.PictureBox4.Tag = ""
        '
        'PictureBox5
        '
        Me.PictureBox5.BackgroundImage = CType(resources.GetObject("PictureBox5.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox5.Location = New System.Drawing.Point(116, 5)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(44, 47)
        Me.PictureBox5.TabIndex = 20
        Me.PictureBox5.TabStop = False
        '
        'lblUserLogIn
        '
        Me.lblUserLogIn.AutoSize = True
        Me.lblUserLogIn.Location = New System.Drawing.Point(269, 23)
        Me.lblUserLogIn.Name = "lblUserLogIn"
        Me.lblUserLogIn.Size = New System.Drawing.Size(85, 13)
        Me.lblUserLogIn.TabIndex = 21
        Me.lblUserLogIn.Text = "Nom usuari login"
        '
        'FrmProjectes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(371, 365)
        Me.Controls.Add(Me.lblUserLogIn)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblUpload)
        Me.Controls.Add(Me.lblDownload)
        Me.Controls.Add(Me.pbxUpload)
        Me.Controls.Add(Me.pbxDownload)
        Me.Controls.Add(Me.lblNew)
        Me.Controls.Add(Me.lblDelete)
        Me.Controls.Add(Me.lstProjectes)
        Me.Name = "FrmProjectes"
        Me.Text = "Llistat de projectes"
        CType(Me.pbxDownload, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxUpload, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstProjectes As System.Windows.Forms.ListView
    Friend WithEvents lblDelete As System.Windows.Forms.Label
    Friend WithEvents lblNew As System.Windows.Forms.Label
    Friend WithEvents pbxDownload As System.Windows.Forms.PictureBox
    Friend WithEvents pbxUpload As System.Windows.Forms.PictureBox
    Friend WithEvents lblDownload As System.Windows.Forms.Label
    Friend WithEvents lblUpload As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUserLogIn As System.Windows.Forms.Label
End Class
